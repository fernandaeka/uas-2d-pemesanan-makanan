package novitasari.fernanda.uas_2d_pemesananmakanan

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_pesan.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class PesanActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var mediaHelper: MediaHelper

  //  lateinit var pesanAdapter : ArrayAdapter<String>

    var daftarPesan = mutableListOf<HashMap<String,String>>()
    lateinit var pesanAdapter : AdapterDataPesan
    var daftarMenu = mutableListOf<String>()
    lateinit var menuAdapter : ArrayAdapter<String> // BINGUNG
    var daftarLevel = mutableListOf<String>()
    lateinit var levelAdapter : ArrayAdapter<String> // BINGUNG
    //var url = "http://192.168.43.47/resto/show_data.php"
    //var url2 = "http://192.168.43.47/resto/get_nama_menu.php"
    //var url3 = "http://192.168.43.47/resto/query_ins_upd_del.php"
    //var url4 = "http://192.168.43.47/resto/get_nama_level.php"
    var url = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/show_data.php"
    var url2 = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/get_nama_menu.php"
    var url3 = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/query_ins_upd_del.php"
    var url4 = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/get_nama_level.php"
    var imStr = ""
    var pilihMenu = ""
    var pilihLevel = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesan)
        pesanAdapter = AdapterDataPesan(daftarPesan, this)
        mediaHelper = MediaHelper(this)
        listPesan.layoutManager = LinearLayoutManager(this)
        listPesan.adapter = pesanAdapter

        menuAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarMenu)
        spinMenu.adapter = menuAdapter
        spinMenu.onItemSelectedListener = itemSelected

        levelAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,daftarLevel)
        listLevel.adapter = levelAdapter
        listLevel.onItemSelectedListener = itemSelected1


        imUpload.setOnClickListener(this)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataPesan()
        getNamaMenu()
        getNamaLevel()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinMenu.setSelection(0)
            pilihMenu = daftarMenu.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMenu = daftarMenu.get(position)
        }

    }
    val itemSelected1 = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {

            listLevel.setSelection(0)
            pilihLevel = daftarLevel.get(0)
       }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
           pilihLevel = daftarLevel.get(position)
        }

    }

//    val itemTouch = object : RecyclerView.OnItemTouchListener{
//        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}
//        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
//            val view = p0.findChildViewUnder(p1.x,p1.y)
//            val tag = p0.getChildAdapterPosition(view!!)
//
//            val pos = daftarProdi.indexOf(daftarMhs.get(tag).get("nama_prodi"))
//            spinProdi.setSelection(pos)
//            edNim.setText(daftarMhs.get(tag).get("nim").toString())
//            edNamaMhs.setText(daftarMhs.get(tag).get("nama").toString())
//            Picasso.get().load(daftarMhs.get(tag).get("url")).into(imUpload)
//
//            return false
//        }
//
//        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}
//
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data!!,imUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataPesan()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_pesan",edIdPesan.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_menu",pilihMenu)
                        hm.put("nama_level",pilihLevel)
                        hm.put("harga",edHarga.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_pesan",edIdPesan.text.toString())
                        hm.put("nama",edNama.text.toString())
                        hm.put("image",imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_menu",pilihMenu)
                        hm.put("nama_level",pilihLevel)
                        hm.put("harga",edHarga.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_pesan",edIdPesan.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaMenu(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarPesan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMenu.add(jsonObject.getString("nama_menu"))
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

   fun getNamaLevel(){
       val request = StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarLevel.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarLevel.add(jsonObject.getString("nama_level"))
                }
                levelAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPesan(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPesan.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pesan = HashMap<String,String>()
                    pesan.put("id_pesan",jsonObject.getString("id_pesan"))
                    pesan.put("nama",jsonObject.getString("nama"))
                    pesan.put("nama_menu",jsonObject.getString("nama_menu"))
                    pesan.put("nama_level",jsonObject.getString("nama_level"))
                    pesan.put("harga",jsonObject.getString("harga"))
                    pesan.put("url",jsonObject.getString("url"))
                    daftarPesan.add(pesan)
                }
                pesanAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })//{
            //override fun getParams(): MutableMap<String, String> {
            //    val hm = HashMap<String,String>()
            //    hm.put("nama",namaPesan)
            //    return hm
        //}
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imUpload ->{
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnInsert ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete ->{
                queryInsertUpdateDelete("delete")
            }
          //  R.id.btnFind ->{
          //      showDataMhs(edNamaMhs.text.toString().trim())
            }
        }
    }
