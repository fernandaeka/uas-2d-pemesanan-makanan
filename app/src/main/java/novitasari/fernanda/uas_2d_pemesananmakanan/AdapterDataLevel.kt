package novitasari.fernanda.uas_2d_pemesananmakanan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_level.*


class AdapterDataLevel(val dataLevel: List<HashMap<String,String>>,
                       val levelActivity: LevelActivity) : //new
    RecyclerView.Adapter<AdapterDataLevel.HolderDataLevel>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataLevel.HolderDataLevel {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_level,p0,false)
        return HolderDataLevel(v)
    }

    override fun getItemCount(): Int {
        return dataLevel.size
    }

    override fun onBindViewHolder(p0: AdapterDataLevel.HolderDataLevel, p1: Int) {
        val data = dataLevel.get(p1)
        p0.txIdLevel.setText(data.get("id_level"))
        p0.txNamaLevel.setText(data.get("nama_level"))

        //beginNew
        if(p1.rem(2) == 0) p0.CLayout3.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.CLayout3.setBackgroundColor(Color.rgb(255,255,245))

        p0.CLayout3.setOnClickListener(View.OnClickListener {
            levelActivity.edIdLevel.setText(data.get("id_level"))
            levelActivity.edLevel.setText(data.get("nama_level"))
        })
        //endNew
    }

    class HolderDataLevel(v: View) : RecyclerView.ViewHolder(v){
        val txIdLevel = v.findViewById<TextView>(R.id.txIdLevel)
        val txNamaLevel = v.findViewById<TextView>(R.id.txNamaLevel)
        val CLayout3 = v.findViewById<ConstraintLayout>(R.id.CLayout3) //new
    }
}