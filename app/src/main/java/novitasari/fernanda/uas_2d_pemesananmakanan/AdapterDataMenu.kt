package novitasari.fernanda.uas_2d_pemesananmakanan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_menu.*


class AdapterDataMenu(val dataMenu: List<HashMap<String,String>>,
                       val menuActivity: MenuActivity) : //new
    RecyclerView.Adapter<AdapterDataMenu.HolderDataMenu>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMenu.HolderDataMenu {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_menu,p0,false)
        return HolderDataMenu(v)
    }

    override fun getItemCount(): Int {
        return dataMenu.size
    }

    override fun onBindViewHolder(p0: AdapterDataMenu.HolderDataMenu, p1: Int) {
        val data = dataMenu.get(p1)
        p0.txIdMenu.setText(data.get("id_menu"))
        p0.txNamaMenu.setText(data.get("nama_menu"))

        //beginNew
        if(p1.rem(2) == 0) p0.cLayout2.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.cLayout2.setBackgroundColor(Color.rgb(255,255,245))

        p0.cLayout2.setOnClickListener(View.OnClickListener {
            menuActivity.edIdMenu.setText(data.get("id_menu"))
            menuActivity.edMenu.setText(data.get("nama_menu"))
        })
        //endNew
    }

    class HolderDataMenu(v: View) : RecyclerView.ViewHolder(v){
        val txIdMenu = v.findViewById<TextView>(R.id.txIdMenu)
        val txNamaMenu = v.findViewById<TextView>(R.id.txNamaMenu)
        val cLayout2 = v.findViewById<ConstraintLayout>(R.id.CLayout2) //new
    }
}