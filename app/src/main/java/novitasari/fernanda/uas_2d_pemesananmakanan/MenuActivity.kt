package novitasari.fernanda.uas_2d_pemesananmakanan

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_menu.*
import org.json.JSONObject
import org.json.JSONArray as JSONArray1


class MenuActivity : AppCompatActivity(), View.OnClickListener {

    //inisiasi variabel
    lateinit var menuAdapter : AdapterDataMenu
    var daftarMenu = mutableListOf<HashMap<String,String>>()
    // var url4 = "http://192.168.43.47/resto/show_data_menu.php"
    //var url5 = "http://192.168.43.47/resto/query_ins_upd_del_menu.php"
    var url4 = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/show_data_menu.php"
    var url5 = "http://192.168.43.228/uas-2d-pemesanan-makanan-web/query_ins_upd_del_menu.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        menuAdapter = AdapterDataMenu(daftarMenu, this)
        listMenu.layoutManager = LinearLayoutManager(this)
        listMenu.adapter = menuAdapter

        btnInsert2.setOnClickListener(this)
        btnUpdate2.setOnClickListener(this)
        btnDelete2.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMenu()
    }

    fun queryInsertUpdateDeleteMenu(mode : String){
        val request = object : StringRequest(
            Method.POST,url5,
            Response.Listener { response ->
                Log.i("info","["+response+"]")
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataMenu()
                }else{
                    Toast.makeText(this,"Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("id_menu",edIdMenu
                            .text.toString())
                        hm.put("nama_menu",edMenu.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_menu",edIdMenu.text.toString())
                        hm.put("nama_menu",edMenu.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_menu",edIdMenu.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMenu(){
        val request = StringRequest(
            Request.Method.POST,url4, //NANTI INI MENYUSUL DIKASIH URL
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray1(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var menu = HashMap<String,String>()
                    menu.put("id_menu",jsonObject.getString("id_menu"))
                    menu.put("nama_menu",jsonObject.getString("nama_menu"))
                    daftarMenu.add(menu)//MENYUSUL
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert2 ->{
                queryInsertUpdateDeleteMenu("insert")
            }
            R.id.btnUpdate2 ->{
                queryInsertUpdateDeleteMenu("update")
            }
            R.id.btnDelete2 ->{
                queryInsertUpdateDeleteMenu("delete")
            }
        }
    }

}