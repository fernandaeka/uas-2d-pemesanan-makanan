package novitasari.fernanda.uas_2d_pemesananmakanan

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pesan.*


class AdapterDataPesan(val dataPesan: List<HashMap<String,String>>,
                     val pesanActivity: PesanActivity) : //new
    RecyclerView.Adapter<AdapterDataPesan.HolderDataPesan>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderDataPesan {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_pesan, p0, false)
        return HolderDataPesan(v)
    }

    override fun getItemCount(): Int {
        return dataPesan.size
    }

    override fun onBindViewHolder(p0: HolderDataPesan, p1: Int) {
        val data = dataPesan.get(p1)
        p0.txIdPesan.setText(data.get("id_pesan"))
        p0.txNama.setText(data.get("nama"))
        p0.txMenu.setText(data.get("nama_menu"))
        p0.txLevel.setText(data.get("nama_level"))
        p0.txHarga.setText(data.get("harga"))

        //beginNew
        if (p1.rem(2) == 0) p0.CLayout.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.CLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.CLayout.setOnClickListener(View.OnClickListener {
            val pos = pesanActivity.daftarMenu.indexOf(data.get("nama_menu"))
            pesanActivity.spinMenu.setSelection(pos)
            val pos1 = pesanActivity.daftarLevel.indexOf(data.get("nama_level"))
            pesanActivity.listLevel.setSelection(pos1)
            pesanActivity.edIdPesan.setText(data.get("id_pesan"))
            pesanActivity.edNama.setText(data.get("nama"))
            pesanActivity.edHarga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(pesanActivity.imUpload)
        })
        //endNew
        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataPesan(v: View) : RecyclerView.ViewHolder(v) {
        val txIdPesan = v.findViewById<TextView>(R.id.txIdPesan)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txMenu = v.findViewById<TextView>(R.id.txMenu)
        val txLevel = v.findViewById<TextView>(R.id.txLevel)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout) //new
    }
}